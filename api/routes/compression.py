"""
Routes
Last modification: 17-06-2023 - Giovanni Junco
"""

from flask import Blueprint, request , make_response
from api.controllers.compression import resolve_file_compression

file_compression = Blueprint('file_compression', __name__)

@file_compression.route("/", methods=["POST"])
def file_compression_post():
    ''' descripcion
        Required attributes:
            user (int): usuario consultante
        Return: 
            response: informacion de 5 categorias'''
    data = request.form.to_dict(flat=True)
    file = request.files.getlist("file")[0]
    data_response= resolve_file_compression(file)
    return make_response(data_response,data_response['status'])
