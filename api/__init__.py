"""
load ap Jellyfin
Last modification: 05-03-2023 - Giovanni Junco
"""
from flask import Flask
from flask_cors import CORS
import api.errors as error
from .routes import file_compression

app = Flask(__name__)
app.register_blueprint(file_compression, url_prefix='/file_compression')

@app.route('/')
def ref():
    ''' Info micro_service'''
    return {
        "Author": "Giovanni Junco",
        "project": "Multimedia Backend",
        "version": "0.1",
        "contributor": ["Giovanni Junco"]
    }

@app.after_request
def after_request(response):
    """ from api.models import database
    database.close() """
    return response

@app.before_request
def before_request():
    ''' start daemon'''
    print('Before')

#Cross-Origin Resource Sharing: integrated access with the front-end
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
#handling unexpected errors like unknown urls - errors.py
error.init_handler(app)
