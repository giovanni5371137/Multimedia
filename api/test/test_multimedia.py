'''
    @author: Giovanni Junco
    @since: 17-08-2023
    @summary: Testing para comprimir archivos
'''

import json
from . import BaseTestClass


class GeneralAuthTestCase(BaseTestClass):
    '''DOCS'''
    ENDPOINT = None
    METHOD = None
    QUERY= None   

#Query mongo
class FileCompression(GeneralAuthTestCase):
    '''
    Testing para comprimir archivos
    '''
    ENDPOINT = '/file_compression/'
    METHOD = 'GET'
    QUERY=  {'page': 1, 'limit': 4,'id':''}
    def test_post_transaction_complete(self):
        '''
        Para este test se evalua la respuesta de la query getPosts
        la información de manera correcta. Se espera un 200        '''
        res = self.client.get(self.ENDPOINT,
                            data=json.dumps(self.QUERY),
                            content_type='application/json')
        self.assertEqual(200, res.status_code, "file_compression/ : file not found")
