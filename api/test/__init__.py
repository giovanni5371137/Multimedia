'''Archivo de configuración para test de servicio
de transacciones'''

import os
import unittest
from api import app


class BaseTestClass(unittest.TestCase):
    '''
    Clase padre
    En esta clase se configura todo el entorno de testing
    '''
    def setUp(self):
        self.app = app
        self.client = self.app.test_client()
