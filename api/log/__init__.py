"""
# Definición del logger root
# -----------------------------------------------------------------------------
# ======================= MAIN SCRIPT ============================================
"""
import logging
logging.basicConfig( level=logging.INFO )

def do_log(tipo, messaje, from_log,type_log):
    ''' save log '''
    log=f'{tipo} Query: {messaje}'
    log+=f' From: {from_log}'
    if type_log=='info':
        logging.info(log)
    elif type_log=='error':
        logging.error(log)
    elif type_log=='critical':
        logging.critical(log)
    elif type_log=='warning':
        logging.warning(log)
