"""
Errors 500
Last modification: 24-04-2023 - Giovanni Junco
"""
from .log import do_log

def get_error(point,error):
    """ log and response error handling 500
        Required attributes:
            point (String): endpoint or resolve
            error(object): data error
        Return: 
            response: to graphql
    """
    do_log(str(error),point,
            error[2].tb_lineno
            ,'error')
    return {"status":"500",
            "message": 'Error: '+point,
            "errors": str(error), "error": str(error) }
