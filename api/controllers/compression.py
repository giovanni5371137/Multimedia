"""
Resolve query categorias
Last modification: 17-06-2023 - Giovanni Junco
"""
import sys

from api.utilities import get_error
from werkzeug.utils import secure_filename
from .helpers.compression import loop_compression

def resolve_file_compression(file):
    """ comprimir archivos
        Required attributes:
            user (int): usuario consultante
        Return: 
            response: informacion de 5 categorias"""
    try:
        filename = secure_filename(file.filename)
        file.save(filename)
        list_compression=[
            loop_compression(filename),''
        ]
        
        return {"data": list_compression, "status":200,
                "msg": 'transaction_completed',
                "error":''}
    except:
        return get_error('resolve_file_compression',sys.exc_info())
