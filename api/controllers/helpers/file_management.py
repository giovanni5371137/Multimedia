"""
Resolve query categorias
Last modification: 17-06-2023 - Giovanni Junco
"""
import sys
import os
import ffmpeg
from moviepy.editor import *
from PIL import Image, ImageOps
from pillow_heif import register_heif_opener
from time import time
from api.utilities import get_error

def get_command(filename, output_name):
    ''' Valida ancho alto y tasa de bit para usar la compresion mas indicada
    -pix_fmt: formato pixel (IOS:yuv420p)
        src_pix_fmt = PIX_FMT_YUV420P;
        dst_pix_fmt1= PIX_FMT_RGB24;
        dst_pix_fmt2= PIX_FMT_GRAY8;
        dst_pix_fmt3= PIX_FMT_RGB8;
    '''
    try:        
        video_info = ffmpeg.probe(filename)
        bitrate=700
        w=1440
        h=1440
        for stream in video_info['streams']:
            if stream['codec_type'] == 'video':
                w=stream['width']
                h=stream['height']
                bitrate = int(stream['bit_rate'])/1000
                break
        print(' -DATA:',bitrate, '*',w,h)
        if bitrate<700 and w<1440 and h<1440:
            return f'ffmpeg -i {filename} -c:v libx264 -pix_fmt yuv420p -profile:v main -level 3.1 -preset medium -crf 30 -x264-params ref=4 -acodec copy -movflags +faststart -y {output_name}'
        else:
            scale=max(w,h)
            scale=scale*0.8 if scale<1440 else 1440
            vf=f'-vf scale={scale}:-2'
            if h>w:
                vf=f'-vf scale=-2:{scale}'
            bv='-b:v 700K'    
            if bitrate<700:
                bitrate=int(bitrate)*0.8
                bv=f'-b:v {bitrate}K'
            print('SIZE:',vf, bv)
            return f'ffmpeg -i {filename} -c:v h264_nvenc {vf} -pix_fmt yuv420p -profile:v main -level 6.2 -preset slow {bv} -x264-params ref=4 -acodec copy -movflags +faststart -y {output_name}'  
    except:
        get_error('movie_compression',sys.exc_info())
    return False    

def nvidia_compression(filename, output_name, crf=''):
    """ librerias reqieridas: ffmpeg, ffmpeg-python """
    try:        
        open_time = time()        
        command=get_command(filename, output_name)
        if os.system(command) ==0:
            close_time = time()
            return close_time-open_time
    except:
        get_error('movie_compression',sys.exc_info())
    return False

def movie_compression(filename, output_name, crf=''):
    """ librerias reqieridas: ffmpeg, ffmpeg-python"""
    try:        
        open_time = time()        
        clip = VideoFileClip(filename)
        clip_resized  = clip.fx( vfx.resize, 0.6)
        clip_resized.write_videofile(output_name)           
        close_time = time()   
        return close_time-open_time
    except:
        get_error('movie_compression',sys.exc_info())
    return False

def ffmpeg_compression(filename, output_name, crf):
    """ comprimir archivos ffmpeg
        Required attributes:
            user (int): usuario consultante
        Return: 
            response: informacion de 5 categorias"""
    try:        
        open_time = time()
        command = f'ffmpeg -i {filename} -c:v libx264 -pix_fmt yuv420p -profile:v main -level 3.1 -preset medium -crf {crf} -x264-params ref=4 -acodec copy -movflags +faststart -y {output_name}'         
        if os.system(command) ==0:
            close_time = time()
            return close_time-open_time
    except:
        get_error('ffmpeg_compression',sys.exc_info())
    return False

def ffmpeg_lib_compression(filename, output_name, crf):
    """ librerias reqieridas: ffmpeg, ffmpeg-python"""
    try:        
        open_time = time() 
        input_audio=ffmpeg.input(filename)
        ffmpeg.output(input_audio, output_name, vcodec='libx264',
                                crf=crf).overwrite_output().run()                
        close_time = time()   
        return close_time-open_time
    except:
        get_error('ffmpeg_lib_compression',sys.exc_info())
    return False