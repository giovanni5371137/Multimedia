"""
Resolve query categorias
Last modification: 17-06-2023 - Giovanni Junco
"""
import sys
import os
from api.utilities import get_error
from .file_management import *

def loop_compression(filename):
    """ comprimir archivos ffmpeg
        Required attributes:
            user (int): usuario consultante
        Return: 
            response: informacion de 5 categorias"""
    try:        
        list_time=[]
        output_name='fin.mp4'  
        crf=30
        for i in range(0,2):    
            list_time.append( nvidia_compression(filename, output_name, crf))
        
        list_time.sort()
        min_time=list_time[0]	
        max_time=list_time[-1]
        average_time= sum(list_time) / len(list_time)
        original_size= os.stat(filename).st_size /1000000
        compressed_size= os.stat(output_name).st_size /1000000
        return [min_time, max_time, average_time, original_size, compressed_size]
    except:
        get_error('loop_compression',sys.exc_info())
    return False
