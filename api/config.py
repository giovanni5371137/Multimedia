"""
Core environment variables
Last modification: 11-02-2023 - Giovanni Junco
"""
import os
from dotenv import load_dotenv

load_dotenv()

development_config={
    'DEBUG' : True,
    ' UPLOAD_FOLDER'  : os.getenv('SECRET_KEY')
}

testing_config={
    'DEBUG' : True,
    ' UPLOAD_FOLDER'  : os.getenv('SECRET_KEY')
}

production_config={
    'DEBUG' : True,
    ' UPLOAD_FOLDER'  : os.getenv('SECRET_KEY')
}

config={
    'development': development_config,
    'testing': testing_config,
    'production': production_config,
    'SECRET_KEY' : os.getenv('SECRET_KEY')
}
