"""
Descripcion servicio
Last modification: 09-02-2023 - Giovanni Junco
"""
from api import app

if __name__ == '__main__':
    #start to all the IP of the VM port 5003
    app.run(debug=True, host="0.0.0.0",port=5003)
