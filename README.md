
# Gloov Multimedia

Compresion de videos en ffmpeg usando GPU



## Installation

Install project with python and virtualenv

```bash
  python -m venv .venv
  .venv\Scripts\activate
  pip install -r requirements.txt
  python app.py

```




## libraries

### ffmpeg
Instalar 
Release ffmpeg git 2023-03-05 builds · GyanD/codexffmpeg (github.com)
Descargar:
ffmpeg-2023-03-05-git-912ac82a3c-essentials_build.zip
descomprimir y copiar a c: con nombre corto lugo agregar a variables de entorno
C:\ffmpeg\bin

### ffmpeg / python
en requeriments
ffmpeg
ffmpeg-python

## pylint

```bash
  python -m pylint .\app.py

  cd api
  pylint_runner
```

## unitest

```bash
  python -m unittest api\test\test_multimedia.py
```

## Tech Stack


**Server:** Python, Flask
