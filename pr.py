import asyncio
import time

async def tarea(nombre, segundos):
    print(f"La tarea {nombre} ha comenzado, Esperando")
    await asyncio.sleep(segundos) #espera de forma asincrona
    print(f"La tarea {nombre} ha finalizado")

async def main():
    tiempo_inicial = time.time()
    tareas= list()
    tareas.append(asyncio.create_task(tarea('plantar mojon',6)))
    tareas.append(asyncio.create_task(tarea('Estudiar mates',5)))

    
    await asyncio.gather(*tareas) #esperar a que todas las tareas asincronas terminan
    total =time.time() - tiempo_inicial
    print(f"Total {total:.2f} segundos")

asyncio.run(main())